# Add New Task

## School Task

新しく学校の課題を登録する時は、下記の2つの場所で登録をすること。

[新規課題登録 (Bitbucket)](https://bitbucket.org/mb21mlschool/default/fork)(新規タブで開かないため、CTRLキー押しながらアクセス)

[新規課題登録 (Dabiyama BOT)](https://docs.google.com/forms/d/e/1FAIpQLSf7KSiskbRnnTRGQzaG1hZofxydW6iS7AJe1HO0bMBb8PvIcw/viewform?entry.1463346092=U9f6e86c1d64c877e800e3b5d2acab97c)(新規タブで開かないため、CTRLキー押しながらアクセス)


なお、現在キューされている新規課題は下記の場所から確認できる。

キューはBOTメールアカウントとBitbucket上の2つから追加可能。

[新規課題キュー](https://bitbucket.org/mb21mlschool/add-new-tasks/issues)


## School Project

課題以外のものを登録する時は、下記の1つの場所で登録をすること。

[新規課題登録 (Bitbucket)](https://bitbucket.org/repo/create?owner=mb21mlschool)(新規タブで開かないため、CTRLキー押しながらアクセス)